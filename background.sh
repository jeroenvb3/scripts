#!/bin/bash

# if test -f "/tmp/background.jpg"; then
#     echo "Image exists already"
# else
    curl -A "background-user-agent" -X GET -L https://www.reddit.com/r/EarthPorn/top\.json\?count=1 | \
        jq ".data.children[0].data.url" | \
        xargs wget --output-document="/tmp/background.jpg" && \
        feh --bg-fill /tmp/background.jpg

    convert /tmp/background.jpg /tmp/background.png
# fi
