import i3ipc
import sys
import os
import logging


logging.basicConfig(filename='/tmp/poly.log', level=logging.DEBUG)

ICON_MAP = [{
    'names': ['discord'],
    'icon': '',
    'weight': 5
}, {
    'names': ['URxvt', 'Gnome-terminal', 'jetbrains-studio', 'st', 'Alacritty'],
    'icon': '',
    'weight': 1
}, {
    'names': ['google_chrome', 'Google-chrome', 'chromium', 'Chromium'],
    'icon': '',
    'weight': 3
}, {
    'names': ['Firefox', 'firefox'],
    'icon': '',
    'weight': 3
}, {
    'names': ['Atom', 'code', 'Code', 'vscode'],
    'icon': '',
    'weight': 1
}, {
    'names': ['Evince'],
    'icon': '',
    'weight': 2
}, {
    'names': ['Pinta', 'Gimp'],
    'icon': '',
    'weight': 3
}, {
    'names': ['DBeaver'],
    'icon': '',
    'weight': 3
}, {
    'names': ['Slack'],
    'icon': '',
    'weight': 3
}, {
    'names': ['Thunderbird'],
    'icon': '',
    'weight': 3
}, {
    'names': ['nomacs', 'Image Lounge'],
    'icon': '',
    'weight': 3
}, {
    'names': ['virt-manager', 'Virt-manager'],
    'icon': '',
    'weight': 3
}, {
    'names': ['STACK', 'Stack', 'stack'],
    'icon': '',
    'weight': 3
}, {
    'names': ['Signal'],
    'icon': '',
    'weight': 3
}, {
    'names': ['Remmina', 'remmina'],
    'icon': 'RDP',
    'weight': 3
}, {
    'names': ['win - VMware Workstation 16 Player (Non-commercial use only)'], # Based on window title instead of class name.
    'icon': '', # This is the windows FA.
    'weight': 3
}, {
    'names': ['vim'], # Based on window title instead of class name.
    'icon': '',
    'weight': 3
}, {
    'names': ['Bluetooth'],
    'icon': '',
    'weight': 3
}, {
    'names': ['Teams'],
    'icon': '',
    'weight': 3
}, {
    'names': ['rubrik-graphql-playground'],
    'icon': 'QL',
    'weight': 3
}]

F_KEYS = ['F1', 'F2', 'F3', 'F4']

backgrounds_colors = {'F1': '#5e8d87', 'F2': '#85678f', 'F3': '#de935f', 'F4': '#8c9440'}


def name_to_values(name):
    try:
        env, section, index = name.split('~')
        return (env, section, index)
    except Exception:
        #  return (0, 0, 0)
        print(f"Could not convert: {name}")
        raise Exception


def get_icon(name):
    try:
        workspace = next((w
                          for w in i3.get_tree().workspaces()
                          if w.name == name))
    except StopIteration:
        return ''

    leaves = workspace.leaves()
    if not leaves:
        icon = ('+', 0)
    else:
        icon = (f'{name_to_values(name)[2]}', -1)

    for c in leaves:
        if icon[1] < 1:
            icon = (c.window_class[0:2], 0)

        for im in ICON_MAP:
            if (c.window_class in im['names'] or c.window_title in im['names'] or im['names'][0] in c.window_title) and icon[1] < im['weight']:
                icon = (im['icon'], im['weight'])

            for name in im["names"]:
                if name in c.window_title and icon[1] < im['weight']:
                    icon = (im['icon'], im['weight'])

    return icon[0]


def print_bar(self=None, e=None):
    bar = {section: '' for section in F_KEYS}
    # Sort workspaces based on [-1]
    sorted_ws = i3.get_workspaces()
    sorted_ws.sort(key=lambda ws: int(ws.name[-1]))

    for ws in sorted_ws:
        logging.info("NAME: " + ws.name)
        logging.info("MON: " + str(os.environ.get('MONITOR')))
        logging.info("OUTPUT: " + ws.output)
        if ws.output != os.environ.get('MONITOR', 'eDP-1'):
            continue
        b = ''
        # Apply color
        if ws.focused:
            b += '%{F#323234}'
        elif ws.urgent:
            b += '%{F#990000}'
        else:
            b += '%{F#FFF}'
        # Apply onclick and set name
        b += '%{{A1:i3-msg workspace {}:}} {} %{{A}}%{{F-}}'.format(ws.name, ws.name[-1] + ": " + get_icon(ws.name))
        try:
            bar[name_to_values(ws.name)[1]] += b
        except Exception as e:
            logging.info("==========: "+ ws.name+ " :===========")
            logging.exception(f"Error: {e}")
            logging.info("==========: "+ ws.name+ " :===========")
            bar['F1'] += b

    # Add group styling
    bar = ['%{{B{} F#FFF}}   {} {}  %{{B- F-}}'.format(backgrounds_colors[section], section, workspaces)
           for section, workspaces in bar.items()]

    print(' '.join(bar))
    sys.stdout.flush()


if __name__ == '__main__':
    i3 = i3ipc.Connection()
    for method in ['focus', 'init', 'empty', 'urgent', 'reload', 'rename', 'restored', 'move']:
        i3.on(f'workspace::{method}', print_bar)

    for method in ['focus', 'close', 'move']:
        i3.on(f'window::{method}', print_bar)

    try:
        print_bar()
        i3.main()
    except KeyboardInterrupt:
        i3.main_quit()
