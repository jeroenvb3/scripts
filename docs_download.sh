#!/bin/sh

wget \ #--wait=2 \
     --level=inf \
	\ #--limit-rate=200K \
	 --recursive \
	 --page-requisites \
	 --user-agent=Mozilla \
	 --no-parent \
	 \ #--convert-links \
	 --adjust-extension \
	 -nc \
	 -e robots=off \
	 -q --show-progress \
	 https://wiki.gentoo.org/wiki/Handbook:AMD64
