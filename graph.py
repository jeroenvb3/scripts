import csv
import matplotlib.pyplot as plt

income = {}
expenses = {}

with open('bank.csv') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        month = row[0][3:]
        if float(row[1]) < 0:
            if month in expenses:
                expenses[month] -= float(row[1])
            else:
                expenses[month] = -float(row[1])
        else:
            if month in income:
                income[month] += float(row[1])
            else:
                income[month] = float(row[1])

# Plot
bar_width = 0.25

# Income
months = income.keys()
amounts = income.values()

i_xpos = [i for i, _ in enumerate(months)]
plt.bar(i_xpos, amounts, bar_width, label='income')

# Expense
months = expenses.keys()
amounts = expenses.values()

e_xpos = [val + bar_width for val in i_xpos]
plt.bar(e_xpos, amounts, bar_width, label='expenses')

# Difference
differences = []

for i, _ in enumerate(income.values()):
    differences.append(list(income.values())[i] - list(expenses.values())[i])

e_xpos = [val + 2 * bar_width for val in i_xpos]
plt.bar(e_xpos, differences, bar_width, label='difference')

# More settings
tick_pos = [val + bar_width / 2 for val in i_xpos]
plt.xticks(tick_pos, months, rotation=45)

plt.title('Income vs Expenses')
plt.legend()
plt.xlabel('months')
plt.ylabel('amounts (euro)')
plt.show()
