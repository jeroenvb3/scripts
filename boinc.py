#!/bin/python
import os

bars = ["▁", "▂", "▃", "▄", "▅", "▆", "▇", "█"]

bashCommand = "boinccmd --get_simple_gui_info | grep 'fraction' | awk '{ print $3}'"
output = os.popen(bashCommand).read()

for i in output.split("\n")[:-1]:
    val = float(i)
    print(bars[int(val * 8)], end=" ")

print()
