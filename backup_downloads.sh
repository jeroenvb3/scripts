#!/usr/bin/env bash

# Variables
BACKUP_REPO="$HOME/Documents/Backup/downloads"
SOURCE_DIR="$HOME/Downloads"

# Export Borg passphrase if using one (replace with your actual passphrase)

# Run backup
borg create --verbose --stats --compression lz4 \
    "$BACKUP_REPO::downloads-$(date +'%Y-%m-%d')" \
    "$SOURCE_DIR"

# Prune backups older than 30 days
borg prune --verbose --list "$BACKUP_REPO" --keep-daily=30

# Find and delete files older than 7 days
find "$SOURCE_DIR" -type f -mtime +7 -exec rm -f {} \;

