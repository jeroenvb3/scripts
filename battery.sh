#!/bin/bash

BATTINFO=`acpi -b`
if [[ `acpi -a | grep off` && `echo $BATTINFO | grep Discharging` && `echo $BATTINFO | cut -f 5 -d " "` < 00:15:00 ]] ; then
    DISPLAY=:0.0 notify-send "low battery" "$BATTINFO"
fi
